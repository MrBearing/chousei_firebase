import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyB_m-HD7ePNSLBnf2bZMFfu5Oq9VexcSFY",
  authDomain: "chousei-firebase-9777d.firebaseapp.com",
  databaseURL: "https://chousei-firebase-9777d.firebaseio.com",
  projectId: "chousei-firebase-9777d",
  storageBucket: "",
  messagingSenderId: "5054579110",
  appId: "1:5054579110:web:35eff5d5f7cf63a1150f67"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
